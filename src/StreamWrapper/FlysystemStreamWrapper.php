<?php

namespace Drupal\flysystem\StreamWrapper;

use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use League\Flysystem\Filesystem;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a StreamWrapper for integrating Flysystem and Drupal.
 *
 * This class provides a stream wrapper implementation to integrate
 * the Flysystem v2 library with Drupal.
 */
class FlysystemStreamWrapper extends Filesystem implements StreamWrapperInterface {

  use StringTranslationTrait;

  /**
   * Instance uri referenced as "<scheme>://key".
   *
   * @var string
   */
  protected $uri = NULL;

  /**
   * Default settings.
   *
   * @var array
   */
  protected $defaults = [
    'driver' => '',
    'config' => [],
    'replicate' => FALSE,
    'cache' => FALSE,
    'name' => '',
    'description' => '',
  ];

  /**
   * Settings for stream wrappers.
   *
   * @var array
   */
  protected $settings = [];

  protected $adapters = [];

  /**
   * Constructs a FlysystemStreamWrapper.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager service.
   */
  public function __construct($stream_wrapper_manager) {
    // The constructor will likely inject an instance of Drupal's
    // StreamWrapperManager so that this interface can be registered
    // appropriately and recognized by Drupal.
    //
    // It will also inject an instance of the Flysystem Adapter being
    // used to interface with the file system or object store associated
    // with the adapter.
    foreach (Settings::get('flysystem', []) as $scheme => $configuration) {

      // The settings.php file could be changed before rebuilding the container.
      if (!$stream_wrapper_manager->isValidScheme($scheme)) {
        continue;
      }
      $this->settings[$scheme] = $configuration + $this->defaults;
      $adapterIdentifier = strtolower($this->settings[$scheme]['driver']);
      $adapter = 'Drupal\flysystem_' . $adapterIdentifier . "\\" . ucfirst($adapterIdentifer) . 'Adapter';
      $this->adapters[$scheme] = $adapter;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getType() {
    return StreamWrapperInterface::NORMAL;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->t('Flysystem Stream Wrapper');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Files served through a registered Flysystem Adapter via the FlysystemStreamWrapper.');
  }

  /**
   * {@inheritdoc}
   */
  public function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    $uri = $this->getUri();
    $path = str_replace('\\', '/', $this->getTarget($uri));
    // @todo Complete implementation of this method.  See notes below.
    //
    // This is the last method left to implement the Drupal StreamWrapperInterface.
    // This works for the S3 Adapter, will not work for Local Adapter.
    // Need to abstract this out.
    //
    // Possible implementation -- write a Drupal wrapper around the Flysystem adapter
    // for each stream wrapper, and implement this method on the Drupal Flysystem
    // adapter.  This should work for both the Local file stream and the AWS S3
    // file stream adapters, as we can then call "getObjectUrl" on the S3 client for the
    // AWS S3 file stream adapter, and write a handler for the Local File Stream
    // adapter.
    //
    // Note:  removing whitespace is probably in order.  The base class, Filesystem,
    // has an implementation of a path normalizer that will remove whitespace and other
    // problematic characters.  It can be called by passing in a path string to the
    // $this->pathNormalizer->normalizePath($path).
    //
    return $this->adapter->getExternalPath($path);
    // S3 implementation??
    //return $this->adapter->client->getObjectUrl();
  }
  /**
   * {@inheritdoc}
   */
  public function realpath() {
    // Not implementing for Flysystem stream wrappers, as this
    // is more applicable for local file systems.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function dirname($uri = NULL) {
    // Not implemented for stream wrappers, as PHP's "dirname()"
    // function does not support stream wrappers.
    return FALSE;
  }

  /**
   * Returns the target file path of a URI.
   *
   * @param string $uri
   *   The URI.
   *
   * @return string
   *   The file path of the URI.
   */
  protected function getTarget($uri) {
    return $this->pathNormalizer->normalizePath(substr($uri, strpos($uri, '://') + 3));
  }

}
