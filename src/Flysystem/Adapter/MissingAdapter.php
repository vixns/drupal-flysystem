<?php

namespace Drupal\flysystem\Flysystem\Adapter;

use League\Flysystem\AdapterInterface;
use League\Flysystem\Config;

/**
 * An adapter used when a plugin is missing. It fails at everything.
 */
class MissingAdapter implements AdapterInterface {

  /**
   * {@inheritdoc}
   */
  public function copy($path, $newpath) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createDirectory($dirname, Config $config) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDir($dirname) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function listContents($directory = '', $recursive = FALSE) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function mimeType($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function fileSize($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function lastModified($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function visibility($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function fileExists($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function directoryExists($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setVisibility($path, $visibility) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function read($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function readStream($path) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function move($path, $newpath) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function write($path, $contents, Config $config) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function writeStream($path, $resource, Config $config) {
    return FALSE;
  }

}
